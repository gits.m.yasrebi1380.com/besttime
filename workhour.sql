-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2019 at 05:30 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `workhour`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `categories` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `categories`, `created_at`, `updated_at`) VALUES
(1, 'asdasdasdasdas', NULL, NULL),
(2, 'asdasdasdasdas', NULL, NULL),
(3, 'kl;j;klhjhjkh', NULL, NULL),
(4, 'fthththgsgdkhjd', NULL, NULL),
(6, 'حسابداری', '2019-08-18 08:10:40', '2019-08-18 08:10:40');

-- --------------------------------------------------------

--
-- Table structure for table `category_time`
--

CREATE TABLE `category_time` (
  `category_id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_time`
--

INSERT INTO `category_time` (`category_id`, `time_id`) VALUES
(1, 19),
(2, 20);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(59, '2014_10_12_000000_create_users_table', 1),
(60, '2014_10_12_100000_create_password_resets_table', 1),
(61, '2019_07_29_124458_create_times_table', 1),
(62, '2019_08_04_143804_create_categories_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `times`
--

CREATE TABLE `times` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `started_at` time NOT NULL,
  `finished_at` time DEFAULT NULL,
  `time_used_at` time DEFAULT NULL,
  `date_day` date NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `time_user`
--

CREATE TABLE `time_user` (
  `time_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time_user`
--

INSERT INTO `time_user` (`time_id`, `user_id`) VALUES
(19, 5),
(20, 5),
(21, 5),
(22, 5),
(23, 5),
(24, 5),
(25, 5),
(26, 5),
(27, 5),
(28, 5),
(29, 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'سید مرتضی', 'a@gmail.com', '$2y$10$6C3fHmftJ6WmBKlH5O4Id.aKSnfwVRXgHRk5Fz6bKC4aQ7KZmEqP6', 2, NULL, '2019-08-09 08:44:52', '2019-08-09 08:50:04'),
(3, 'q223rqfwgwew', 'rteza@gmail.com', '$2y$10$drFNG3JCfPYCbGdRbqDWreju7IFjP1Qi9gB7qZFs0N7bhXEef0nAi', 2, NULL, '2019-08-09 08:53:46', '2019-08-09 08:53:46'),
(4, 'سید ', 's@gmail.com', '$2y$10$3XqNl25vHTaZbLOAnSjWC.RpUFn9D3QiuSCpDUo9y1AmJEtNO0BEK', 1, NULL, '2019-08-09 08:54:48', '2019-08-09 08:54:48'),
(5, 'سید مرتضی یثربی', 'yasrebi@gmail.com', '$2y$10$rLPtETt3dM7gzN9wj3NVE.Q70whaWiklsBqhBg02Q1y.sYPe9Ib22', 2, 'GAtoRa97kyOiRc8hTra1MrsGgM13jDCifycSaWHxf9etu07DOJTpFWYGiSvS', '2019-08-09 09:09:56', '2019-08-24 05:21:15'),
(6, 'q223rqfwgwew', 'aasdasdasd@gmail.com', '$2y$10$1.aDD3hpNY26q/kpbPcb6eRlCy1vDZ6OFwenXrtJo2yma0o/2FR0C', 1, NULL, '2019-08-09 09:11:16', '2019-08-09 09:11:16'),
(8, 'سید مرتضی', 'morteza@gmail.com', '$2y$10$XNi9V2ZNmVjIGBWIIEnUMu1sy3fI.ZYNcpdyCabLe.R4Kxxv65PWW', 1, 'uQCMj7vjZLBJRhST4HwcaJCwbsqKTmcNN2OVCUdu7XzlAxfEFmlO3XO1qE1q', '2019-08-14 07:25:51', '2019-08-18 07:46:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_time`
--
ALTER TABLE `category_time`
  ADD PRIMARY KEY (`category_id`,`time_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `times`
--
ALTER TABLE `times`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `time_user`
--
ALTER TABLE `time_user`
  ADD PRIMARY KEY (`time_id`,`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `times`
--
ALTER TABLE `times`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
