<?php

namespace App;

use App\Models\Category;
use App\Models\Time;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;


//
//    protected $fillable = [
//        'name', 'email', 'password',
//    ];


    const USER = 1;
    const ADMIN = 2;

    protected $guarded = ['id'];


    protected $hidden = [
        'password', 'remember_token',
    ];


    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }


    public function times()
    {
        return $this->belongsToMany(Time::class);
    }


    /*public function categories()
    {
        return $this->belongsToMany(Category::class);
    }*/


}
