<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $guarded = ['id'];

    const PARTIAL = 1;
    const FINISHED = 2;

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }


}
