<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = ['id'];


    /*public function user()
    {
        return $this->belongsToMany(User::class);
    }*/

    public function times()
    {
        return $this->belongsToMany(Time::class);
    }


}
