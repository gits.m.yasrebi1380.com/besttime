<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class dashboardController extends Controller
{
    public function index(Request $request, $user_id)
    {
        if (Auth::check()) {
            $user = array($user_id);
            $user_item = User::find($user);
            return view('admin.dashboard.dashboard', compact('user_item'))->with('panel_title', 'ویرایش اکانت');
        }
        return redirect('login')->with('error','لطفا ابتدا وارد شوید');
    }
}
