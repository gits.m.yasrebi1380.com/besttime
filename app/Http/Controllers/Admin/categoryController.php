<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Time;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class categoryController extends Controller
{
    public function index()
    {
        $category = Category::all();
        return view('admin.category.index', compact('category'))->with(['panel_title' => 'لیست دسته بندی ها']);
    }

    public function create()
    {
        return view('admin.category.create')->with(['panel_title' => 'ایجاد دسته بندی']);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'full_name' => 'required'
        ], [
            'full_name.required' => 'وارد کردن نام دسته بندی الزامی می باشد'
        ]);


        /*dd(request()->all());*/
        $category_data = [

            'categories' => request()->input('full_name'),

        ];

        $new_category_object = Category::create($category_data);
        if ($new_category_object instanceof Category) {
            return redirect()->route('admin.category.index')->with(['success' => 'دسته بندی مورد نظر با موفقیت ایجاد گردید']);
        }

    }

    public function delete($category_id)
    {
        if ($category_id && ctype_digit($category_id)) {
            $categoryItem = Category::find($category_id);
            if ($categoryItem && $categoryItem instanceof Category) {
                $categoryItem->delete();
                return redirect()->route('admin.category.index')->with('success', 'دسته بندی مورد نظر با موفقیت حذف گردید.');
            }
        }
    }

    public function edit($category_id)
    {
        if ($category_id && ctype_digit($category_id)) {
            $categoryItem = Category::find($category_id);
            if ($categoryItem && $categoryItem instanceof Category) {
                return view('admin.category.edit', compact('categoryItem'))->with(['panel_title' => 'ویرایش دسته بندی']);
            }
        }
    }

    public function update(Request $request, $category_id)
    {

        $this->validate($request, [
            'full_name' => 'required'
        ], [
            'full_name.required' => 'وارد کردن نام دسته بندی الزامی می باشد'
        ]);

        $inputs = [
            'categories' => request()->input('full_name'),

        ];

        $categoryItem = Category::find($category_id);
        $categoryItem->update($inputs);
        return redirect()->route('admin.category.index')->with('success', 'اطلاعات دسته بندی مورد نظر با موفقیت به روز رسانی شد.');

    }

    public function category_time(Request $request,$time_id)
    {
        $time_item = Time::find($time_id);
        $category_time = $time_item->categories()->get();
        return view('admin.category.category_time', compact('category_time'))->with(['panel_title' => 'دسته بندی مربوط به این زمان']);
    }


}
