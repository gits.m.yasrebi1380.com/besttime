<?php

namespace App\Http\Controllers\Admin;

use App\Models\Time;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class timeController extends Controller
{
    public function index(Request $request,$user_id)
    {
        //$time = Time::all();
        $user_item = User::find($user_id);
        $time_user = $user_item->times()->get();
        return view('admin.time.index', compact('time_user'))->with(['panel_title' => 'لیست زمان بندی کاربر']);
    }

    public function delete($time_id)
    {
        if ($time_id && ctype_digit($time_id)) {
            $timeItem = Time::find($time_id);
            if ($timeItem && $timeItem instanceof Time) {
                $timeItem->delete();
                return redirect()->route('admin.time.index')->with('success', 'زمان بندی مورد نظر با موفقیت حذف گردید.');
            }
        }
    }

    public function edit($time_id)
    {
        if ($time_id && ctype_digit($time_id)) {
            $timeItem = Time::find($time_id);
            if ($timeItem && $timeItem instanceof Time) {
                return view('admin.time.edit', compact('timeItem'))->with(['panel_title' => 'ویرایش زمان بندی']);
            }
        }
    }

    public function update(Request $request, $time_id)
    {

        $this->validate($request, [
            'started_at' => 'required',
            'finished_at' => 'required',
            'time_used_at' => 'required',
            'date_day' => 'required',
            'description' => 'required',
        ], [
            'started_at.required' => 'وارد کردن زمان شروع الزامی می باشد',
            'finished_at.required' => 'وارد کردن زمان پایان الزامی می باشد',
            'time_used_at.required' => 'وارد کردن زمان استفاده شده الزامی می باشد',
            'date_day.required' => 'وارد کردن تاریخ الزامی می باشد',
            'description.required' => 'وارد کردن توضیحات مربوطه الزامی می باشد',
        ]);

        $inputs = [
            'started_at' => request()->input('started_at'),
            'finished_at' => request()->input('finished_at'),
            'time_used_at' => request()->input('time_used_at'),
            'date_day' => request()->input('date_day'),
            'description' => request()->input('description'),
        ];

        $timeItem = Time::find($time_id);
        $timeItem->update($inputs);
        return redirect()->route('admin.user.index')->with('success', 'اطلاعات زمان بندی مورد نظر با موفقیت به روز رسانی شد.');

    }

}
