<?php

namespace App\Http\Controllers\Frontend;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class userController extends Controller
{

    public function login()
    {
        return view('frontend.user.login');
    }

    public function register()
    {
        return view('frontend.user.register');
    }

    public function doLogin(Request $request)
    {

        $this->validate($request, [

            'email' => 'required|email',
            'password' => 'required|min:6|max:12',

        ], [

            'email.required' => 'وارد کردن ایمیل الزامی می باشد.',
            'email.email' => 'ایمیل وارد شده معتبر نمی باشد.',
            'password.required' => 'وارد کردن کلمه عبور الزامی می باشد.',
            'password.min' => 'کلمه عبور حداقل 6 کاراکتر نیاز دارد.',
            'password.max' => 'کلمه عبور حداکثر باید 12 کاراکتر باشد.',

        ]);

        $remember = $request->has('remember');

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')],$remember)) {
            return redirect('/')->with('success', 'کاربر عزیز شما وارد شدید');
        }

        return redirect()->back()->with('error', 'نام کاربری یا کلمه ی عبور اشتباه وارد شده');


    }

    public function doRegister(Request $request)
    {

        $this->validate($request, [

            'full_name' => 'required' ,
            'email' => 'required|email',
            'password' => 'required|min:6|max:12',

        ], [

            'full_name.required' => 'وارد کردن نام کامل الزامی می باشد.' ,
            'email.required' => 'وارد کردن ایمیل الزامی می باشد.',
            'email.email' => 'ایمیل وارد شده معتبر نمی باشد.',
            'password.required' => 'وارد کردن کلمه عبور الزامی می باشد.',
            'password.min' => 'کلمه عبور حداقل 6 کاراکتر نیاز دارد.',
            'password.max' => 'کلمه عبور حداکثر باید 12 کاراکتر باشد.',

        ]);

        $new_user_data = [
            'name' => $request->input('full_name'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];

        $new_user = User::create($new_user_data);

        if ($new_user && $new_user instanceof User){

            return redirect('/')->with('success','فرآیند ثبت نام با موفقیت انجام گردید.');

        }

        return redirect()->back()->with('success','در فرآیند ثبت نام مشکلی رخ داده لطفا بعدا امتحان کنید.');

    }


    public function logout()
    {
        Auth::logout();
        return redirect('/')->with('success','شما خارج شده اید');
    }

}
