<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Time;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use MongoDB\BSON\UTCDateTime;

class timeController extends Controller
{
    public function index(Request $request, $user_id)
    {
        $user_item = User::find($user_id);
        $time_user = $user_item->times()->get();
        return view('frontend.time.index', compact('time_user'))->with(['panel_title' => 'لیست زمان بندی شما']);
    }

    public function create()
    {
        $category_item = Category::all();
        return view('frontend.time.form', compact('category_item'))->with(['panel_title' => 'ثبت زمان بندی']);
    }

    public function start(Request $request)
    {
        $this->validate($request, [

            'started_at' => 'required',
            'date_day' => 'required',

        ], [

            'started_at.required' => 'وارد کردن ساعت شروع الزامی می باشد.',
            'date_day.required' => 'وارد کردن تاریخ روز الزامی می باشد.',

        ]);

        $new_time_data = [

            'category_id' => $request->input('category_id'),
            'started_at' => $request->input('started_at'),
            'date_day' => $request->input('date_day'),
            'user_id' => Auth::user()->id,
        ];

        $new_time = Time::create($new_time_data);
        if ($new_time && $new_time instanceof Time) {
            return view('frontend.time.form_finish', compact('new_time'));
        }
        return redirect()->back()->with('error', 'در فرایند زمان شروع مشکلی رخ داده لطفا بعدا امتحان کنید');

    }

    public function finish()
    {
        return view('frontend.time.form_finish', compact('time_item'));
    }

    public function finish2(Request $request, $time_id)
    {

        $this->validate($request, [

            'finished_at' => 'required',
            'description' => 'required',

        ], [

            'finished_at.required' => 'وارد کردن ساعت پایان الزامی می باشد.',
            'description.required' => 'وارد کردن توضیحات الزامی می باشد.',

        ]);

        $start = $request->input('started_at');
        $start2 = Carbon::createFromTimeString($start);
        $finish = $request->input('finished_at');
        $finish2 = Carbon::createFromTimeString($finish);
        $diff_in_time = $start2->diffInHours($finish2);

        $new_time_data = [
            'finished_at' => $request->input('started_at'),
            'description' => $request->input('description'),
            'time_used_at' => $diff_in_time,
            'status' => '2'
        ];


        $timeItem = Time::find($time_id);
        $timeItem->update($new_time_data);
        $user_id = Auth::user()->id;
        $user_item = User::find($user_id);
        $time_user = $user_item->times()->get();
        if ($timeItem && $timeItem instanceof Time) {
            return view('frontend.time.index', compact('time_user'))->with('success', 'ثبت زمان با موفقیت انجام شد');
        }
        return redirect()->back()->with('error', 'در فرایند ثبت زمان پایان مشکلی رخ داده لطفا بعدا امتحان کنید');

    }


    /*
        public function delete(Request $request, $time_id)
        {
            if ($time_id && ctype_digit($time_id)) {
                $timeItem = Time::find($time_id);
                if ($timeItem && $timeItem instanceof Time) {
                    $timeItem->delete();
                    return redirect()->route('admin.time.index')->with('success', 'زمان بندی مورد نظر با موفقیت حذف گردید.');
                }
            }
        }*/

    public function category_time(Request $request, $time_id)
    {
        $time_item = Time::find($time_id);
        $category_time = $time_item->categories()->get();
        return view('frontend.time.category_time', compact('category_time'))->with(['panel_title' => 'دسته بندی مربوط به این زمان']);
    }

    public function create2()
    {
        $category = Category::all();
        return view('frontend.time.form_edit', compact('category'))->with('panel_title', 'ثبت دستی زمان');
    }

    public function store(Request $request)
    {
        $this->validate($request, [

            'finished_at' => 'required',
            'started_at' => 'required',
            'time_used_at' => 'required',
            'date_day' => 'required',
            'description' => 'required',

        ], [

            'finished_at.required' => 'وارد کردن ساعت پایان الزامی می باشد.',
            'started_at.required' => 'وارد کردن ساعت شروع الزامی می باشد.',
            'time_used_at.required' => 'وارد کردن زمان صرف شده الزامی می باشد.',
            'date_day.required' => 'وارد کردن تاریخ الزامی می باشد.',
            'description.required' => 'وارد کردن توضیحات الزامی می باشد.',

        ]);

        $start = $request->input('started_at');
        $start2 = Carbon::createFromTimeString($start);
        $finish = $request->input('finished_at');
        $finish2 = Carbon::createFromTimeString($finish);
        $diff_in_time = $start2->diffInHours($finish2);

        $new_time_data = [
            'started_at' => $request->input('started_at'),
            'finished_at' => $request->input('finished_at'),
            'description' => $request->input('description'),
            'time_used_at' => $diff_in_time,
            'date_day' => $request->input('date_day'),
            'status' => '2',
            'user_id' => Auth::user()->id,
            'category_id' => $request->input('category')
        ];
        $new_time = Time::create($new_time_data);
        $user_id = Auth::user()->id;
        $user_item = User::find($user_id);
        $time_user = $user_item->times()->get();
        if ($new_time && $new_time instanceof Time) {
            return view('frontend.time.index', compact('time_user'))->with('success', 'ثبت زمان با موفقیت انجام شد');
        }
        return redirect()->back()->with('error', 'در فرایند ثبت زمان پایان مشکلی رخ داده لطفا بعدا امتحان کنید');

    }

}
