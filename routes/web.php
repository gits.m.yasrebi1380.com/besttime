<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\homeController@index')->name('home');


Route::group(['namespace' => 'Frontend'], function () {


    // Users auth

    Route::get('login', 'userController@login')->name('login');
    Route::post('login', 'userController@doLogin')->name('doLogin');
    Route::get('register', 'userController@register')->name('register');
    Route::post('register', 'userController@doRegister')->name('doRegister');
    Route::get('logout', 'userController@logout')->name('logout');


    // times Routes

    Route::get('/category/{time_id}', 'timeController@category_time')->name('frontend.category.category_time');
    Route::get('/time/create', 'timeController@create')->name('frontend.time.create');
    Route::post('/time/create', 'timeController@start')->name('frontend.time.start');
    Route::get('/time/create/2', 'timeController@create2')->name('frontend.time.create2');
    Route::post('/time/create/2', 'timeController@store')->name('frontend.time.store');
    Route::get('/time/finish', 'timeController@finish')->name('frontend.time.finish');
    Route::post('/time/finish/{time_id}', 'timeController@finish2')->name('frontend.time.finish2');
    /*Route::get('/time/delete/{time_id}', 'timeController@delete')->name('frontend.time.delete');*/
    Route::get('/time/edit/{time_id}', 'timeController@edit')->name('frontend.time.edit');
    Route::post('/time/edit/{time_id}', 'timeController@update')->name('frontend.time.update');
    Route::get('/time/{user_id}', 'timeController@index')->name('frontend.time.index');


});


Route::group(['prefix' => 'admin', 'namespace' => 'admin', 'middleware' => 'admin'], function () {


    Route::get('/{user_id}', 'dashboardController@index')->name('admin.dashboard');


    // Users Routes

    Route::get('/user/index', 'userController@index')->name('admin.user.index');
    Route::get('/user/create', 'userController@create')->name('admin.user.create');
    Route::post('/user/create', 'userController@store')->name('admin.user.store');
    Route::get('/user/delete/{user_id}', 'userController@delete')->name('admin.user.delete');
    Route::get('/user/edit/{user_id}', 'userController@edit')->name('admin.user.edit');
    Route::post('/user/edit/{user_id}', 'userController@update')->name('admin.user.update');


    // times Routes

    Route::get('/time/{user_id}', 'timeController@index')->name('admin.time.index');
    Route::get('/time/delete/{time_id}', 'timeController@delete')->name('admin.time.delete');
    Route::get('/time/edit/{time_id}', 'timeController@edit')->name('admin.time.edit');
    Route::post('/time/edit/{time_id}', 'timeController@update')->name('admin.time.update');

    // Categories Routes

    Route::get('/category/index', 'categoryController@index')->name('admin.category.index');
    Route::get('/category/create', 'categoryController@create')->name('admin.category.create');
    Route::post('/category/create', 'categoryController@store')->name('admin.category.store');
    Route::get('/category/{time_id}', 'categoryController@category_time')->name('admin.category.category_time');
    Route::get('/category/delete/{category_id}', 'categoryController@delete')->name('admin.category.delete');
    Route::get('/category/edit/{category_id}', 'categoryController@edit')->name('admin.category.edit');
    Route::post('/category/edit/{category_id}', 'categoryController@update')->name('admin.category.update');


});
