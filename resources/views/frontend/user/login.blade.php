@extends('layouts.frontend')
@section('content')
    <div  style="display: grid !important;">
        @include('frontend.partials.errors')
        <div class="col-xs-10 col-md-4" style="width: 700px;
    text-align: right;
    font-size: large;
    color: #000000">
            <form action="{{ route('doLogin') }}" method="post">
                {{ csrf_field()  }}

                <div class="form-group">
                    <label for="email">ایمیل :</label>
                    <input type="email" class="form-control" name="email" id="email">
                </div>
                <div class="form-group">
                    <label for="password">کلمه عبور :</label>
                    <input type="password" class="form-control" name="password" id="password">
                </div>

                <div class="form-group">
                    <div class="checkbox">
                        <label><input type="checkbox" name="remember">مرا بخاطر بسپار</label>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-success" type="submit">ورود</button>
                </div>
            </form>

            <span> آیا هنوز ثبت نام نکرده اید ؟ </span>
            <a class="nav-link" href="{{ route('register') }}">
                <button class="btn btn-outline-primary">ثبت نام</button>
            </a>

        </div>
    </div>

@endsection