@extends('layouts.frontend')
@section('content')
    <div style="display: grid !important;">
        @include('admin.partials.errors')
        <div class="col-xs-10 col-md-4" style="width: 700px;
    text-align: right;
    font-size: large;
    color: #000000">
            <form action="{{ route('doRegister') }}" method="post">
                {{ csrf_field()  }}
                <div class="form-group">
                    <label for="full_name">نام و نام خانوادگی :</label>
                    <input type="text" class="form-control " name="full_name" id="full_name"
                           value="{{ old('full_name',isset($userItem) ? $userItem->name: '')  }}">
                </div>
                <div class="form-group">
                    <label for="email">ایمیل :</label>
                    <input type="email" class="form-control " name="email" id="email"
                           value="{{ old('email',isset($userItem) ? $userItem->email: '')  }}">
                </div>
                <div class="form-group">
                    <label for="password">کلمه عبور :</label>
                    <input type="password" class="form-control " name="password" id="password">
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">ذخیره اطلاعات</button>
                </div>
            </form>

            <span> ورود باحساب های دیگر : </span>
            <a class="nav-link" href="#">
                <button class="btn btn-outline-primary">بزودی</button>
            </a>

        </div>
    </div>
@endsection