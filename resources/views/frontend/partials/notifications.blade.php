@if(session('success'))
    <div class="alert alert-success" style="font-size: x-large;text-align: center;">
        <p>{{ session('success') }}</p>
    </div>
@endif