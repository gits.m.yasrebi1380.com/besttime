@if($errors->any())
    <div class="alert alert-danger" style="font-size: x-large;text-align: right;">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@elseif(session('error'))
    <div class="alert alert-danger" style="font-size: x-large;text-align: right;">
        <p>{{ session('error') }}</p>
    </div>
@endif
