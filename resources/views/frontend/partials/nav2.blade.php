{{--          MAIN MENU          --}}
<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a class="nav-link" href="/">
        <button class="btn btn-dark w-100">صفحه ی اصلی</button>
    </a>
    @if(!\Illuminate\Support\Facades\Auth::check())
        <a class="nav-link" href="{{ Route('register') }}">
            <button class="btn btn-dark w-100">ثبت نام</button>
        </a>
        <a class="nav-link" href="{{ Route('login') }}">
            <button class="btn btn-dark w-100">ورود</button>
        </a>
    @else
        <a class="nav-link"
           href="{{ route('admin.dashboard', \Illuminate\Support\Facades\Auth::user()->id ) }}">
            <button class="btn btn-dark w-100">پنل مدیریت</button>
        </a>

        <a class="nav-link"
           href="{{ route('frontend.time.index', \Illuminate\Support\Facades\Auth::user()->id ) }}">
            <button class="btn btn-dark w-100">لیست زمان بندی شما</button>
        </a>

    @endif
</div>


<div id="main" style="float: right;z-index: 2000;position: fixed;margin-top: -10px;">
    <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; منوی کاربری </span>
</div>
{{--          MAIN MENU          --}}


{{--          RESPONSIVE MENU          --}}

<div id="mySidenav2" class="sidenav2">
    <a href="javascript:void(0)" class="closebtn2" onclick="closeNav2()">&times;</a>
    <a class="nav-link" href="/">
        <button class="btn btn-dark w-100">صفحه ی اصلی</button>
    </a>
    @if(!\Illuminate\Support\Facades\Auth::check())
        <a class="nav-link" href="{{ Route('register') }}">
            <button class="btn btn-dark w-100">ثبت نام</button>
        </a>
        <a class="nav-link" href="{{ Route('login') }}">
            <button class="btn btn-dark w-100">ورود</button>
        </a>
    @else


        <p>
            <a class="btn btn-dark dropdown-toggle w-100" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">{{ \Illuminate\Support\Facades\Auth::user()->name}}</a>
        </p>
        <div class="row">
            <div class="col">
                <div class="collapse multi-collapse" id="multiCollapseExample1">
                    <button class="btn btn-dark w-100">
                            <a class="dropdown-item text-white" href="{{ route('logout') }}">
                                خروج
                            </a>
                    </button>
                </div>
            </div>
        </div>

        <a class="nav-link"
           href="{{ route('admin.dashboard', \Illuminate\Support\Facades\Auth::user()->id ) }}">
            <button class="btn btn-dark w-100">پنل مدیریت</button>
        </a>

        <a class="nav-link"
           href="{{ route('frontend.time.index', \Illuminate\Support\Facades\Auth::user()->id ) }}">
            <button class="btn btn-dark w-100">لیست زمان بندی شما</button>
        </a>

    @endif
</div>


<div id="main2" class="res_menu" style="float: right;z-index: 2000;position: fixed;">
    <span style="font-size:30px;cursor:pointer" onclick="openNav2()">&#9776; منو </span>
</div>

{{--          RESPONSIVE MENU          --}}




