@extends('layouts.frontend')
@section('content')

    @include('frontend.partials.notifications')


    @if($category_time && count($category_time) > 0)
        <table class="table table-bordered text-right">
            <thead class="btn-primary">
            <tr>
                <th class="text-right">شناسه</th>
                <th class="text-right">نام</th>
            </tr>
            </thead>
            @foreach($category_time as $categor)
                @include('frontend.time.category_time_item',$categor)
            @endforeach
        </table>

    @else

        <thead class="btn-primary">
        <tr>
            <th class="text-right">شناسه</th>
            <th class="text-right">نام</th>
        </tr>
        </thead>
        <tr>
            <td>هیچ دسته بندی ای برای این زمان موجود نیست</td>
        </tr>

    @endif

    <a class="nav-link"
       href="{{ route('frontend.time.index', \Illuminate\Support\Facades\Auth::user()->id ) }}">
        <button class="btn btn-warning">بازگشت</button>
    </a>

@endsection

