@extends('layouts.frontend')
@section('content')
    <div class="row" style="display: grid !important;">
        @include('frontend.partials.errors')
        <div class="col-xs-10 col-md-8" style="width: 700px;
    text-align: right;
    font-size: large;
    color: white">

            <form action="" method="post">
                {{ csrf_field()  }}
                <div style="width: 50%">
                    <div class="form-group">
                        <label for="started_at">زمان شروع :</label>
                        <input type="time" class="form-control " name="started_at" id="started_at"
                               value="{{ old('started_at',isset($timeItem) ? $timeItem->started_at: '')  }}">
                    </div>
                    <div class="form-group">
                        <label for="finished_at">زمان پایان :</label>
                        <input type="time" class="form-control " name="finished_at" id="finished_at"
                               value="{{ old('finished_at',isset($timeItem) ? $timeItem->finished_at: '')  }}">
                    </div>
                    <div class="form-group">
                        <label for="time_used_at">زمان صرف شده :</label>
                        <input type="time" class="form-control " name="time_used_at"
                               id="time_used_at"
                               value="{{ old('time_used_at',isset($timeItem) ? $timeItem->time_used_at: '')  }}">
                    </div>
                    <div class="form-group">
                        <label for="date_day">تاریخ :</label>
                        <input type="date" class="form-control " name="date_day" id="date_day"
                               value="{{ old('date_day',isset($timeItem) ? $timeItem->date_day: '')  }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">توضیحات مربوطه :</label>
                    <textarea name="description" style="width: 100%"
                              id="description">{{ old('description',isset($timeItem) ? $timeItem->description: '')  }}</textarea>

                    {{--<input type="text" class="form-control btn-outline-primary" name="description" id="description"
                            value="{{ old('description',isset($timeItem) ? $timeItem->description: '')  }}">--}}
                </div>

                <div class="form-group">
                    <button class="btn btn-success" type="submit">ذخیره اطلاعات</button>
                </div>
            </form>
        </div>
    </div>
@endsection