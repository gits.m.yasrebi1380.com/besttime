@extends('layouts.frontend')
@section('content')



    <form action="" method="post">
        {{ csrf_field() }}

        <div style="width: 50%">

            <div class="form-group">
                <label for="category">دسته بندی مورد نظر را انتخاب کنید:</label>
                <select class="form-control" name="category_id" id="category">
                    @foreach($category_item as $category)
                        <option value="{{  $category->id }}">{{  $category->categories }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <input type="time" class="form-control " name="started_at" id="started_at"
                       value="{{ Carbon\Carbon::now('TMT')->toTimeString() }}">
            </div>

            <div class="form-group">
                <input type="date" class="form-control " name="date_day" id="date_day"
                       value="{{ Carbon\Carbon::now()->toDateString() }}">
            </div>

            <a href="">
                <button class="btn btn-success .button-circle">شروع</button>
            </a>

        </div>
    </form>



@endsection