@extends('layouts.frontend')
@section('content')
    <div class="row" style="display: grid !important;">
        @include('admin.partials.errors')
        <div class="col-xs-10 col-md-8" style="width: 700px;
    text-align: right;
    font-size: large;">
                <form action="{{ route('frontend.time.finish2', $new_time->id ) }}" method="post">
                    {{ csrf_field()  }}
                    <div style="width: 50%">
                        <div class="form-group hidden">
                            <input type="time" class="form-control" name="started_at" value="{{ $new_time->started_at }}">
                        </div>

                        <div class="form-group">
                            <label for="finished_at">زمان پایان :</label>
                            <input type="time" class="form-control" name="finished_at" id="finished_at"
                                   value="{{ Carbon\Carbon::now('TMT')->toTimeString() }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">توضیحات مربوطه :</label>
                        <textarea name="description" style="width: 100%"
                                  id="description"></textarea>

                        {{--<input type="text" class="form-control btn-outline-primary" name="description" id="description"
                                value="{{ old('description',isset($timeItem) ? $timeItem->description: '')  }}">--}}
                    </div>

                    <a href="">
                        <button type="submit" class="btn btn-danger">پایان</button>
                    </a>

                </form>
        </div>
    </div>
@endsection