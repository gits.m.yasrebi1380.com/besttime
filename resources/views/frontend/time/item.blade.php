<tr>
    <td>{{ $tim->started_at  }}</td>
    <td>{{ $tim->finished_at  }}</td>
    <td>{{ $tim->time_used_at  }}</td>
    <td>{{ $tim->date_day  }}</td>
    <td class="text-break">{{ $tim->description  }}</td>
    <td>
        @if( $tim->status == \App\Models\Time::FINISHED )
            <button class="btn btn-success w-100">تمام شده</button>
        @else
            <button class="btn btn-danger w-100" data-toggle="tooltip" data-placement="top" title="برای تکمیل لطفا از قسمت عملیات اقدام کنید">
                ناتمام
            </button>
        @endif
    </td>
    <td style="text-align: center;">
        @include('frontend.time.operation',$tim)
    </td>
</tr>