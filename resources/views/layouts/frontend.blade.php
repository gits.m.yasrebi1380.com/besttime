<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Work Hour</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/bootstrap-rtl.min.css" rel="stylesheet">
    <link href="/css/frontend.css" rel="stylesheet">
    <link href="/css/bootstrap-grid.css" rel="stylesheet">
    <link href="/css/bootstrap-reboot.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">

</head>


<style>
    table {
        color: #000000 !important;
    }

    .panel {
        color: #000000 !important;
    }

    .button-circle {
        border-radius: 30px;
        height: 50px;
    }

    input.btn-primary {
        color: #000000 !important;
    }

    .nav-fix {
        background-color: #ffffff;
        position: fixed;
        z-index: 1002;
        width: 100%;
    }

    .my-85 {
        margin-top: 85px;
    }

    .logo {
        margin-right: 200px;
    }

    .logo2 {
        margin-right: -25px;
    }

    .margin {
        margin: 0 220px 0 20px !important;
        transition: 0.5s;
    }

    .sidenav2 {
        display: none;
    }

    .closebtn2 {
        display: none;
    }

    div.res_data_user {
        display: none;
    }

    #main2 {
        display: none;
    }

    hr {
        display: none;
    }

    /* ------------------------ MENU USERS --------------------------- */

    .sidenav {
        height: 100%;
        width: 200px;
        position: fixed;
        z-index: 1000;
        top: 0;
        background-color: #111;
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 105px;
        text-align: right;
        direction: rtl;
    }

    .sidenav a {
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 25px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }

    .sidenav a:hover {
        color: #f1f1f1;
    }

    .sidenav .closebtn {
        position: absolute;
        top: 53px;
        left: 0;
        font-size: 36px;
        margin-left: -20px;
    }

    #main {
        transition: margin-left .5s;
        padding: 16px;
    }

    @media screen and (max-height: 450px) {
        .sidenav {
            padding-top: 110px;
        }

        .sidenav a {
            font-size: 18px;
        }
    }

    /* ------------------------ MENU USERS --------------------------- */

    /* ------------------------ RESPONSIVE --------------------------- */

    @media screen and (max-width: 500px) {

        /* ------------- MENU RESPONSIVE ---------------- */
        .sidenav {
            display: none;
        }

        .logo, .logo2 {
            display: none;
        }

        .sidenav .closebtn {
            display: none;
        }

        #main {
            display: none;
        }

        .sidenav2 .closebtn2 {
            position: relative;
            top: 2;
            /* left: -146px; */
            right: 333px;
            font-size: 36px;
            margin-left: -20px;
            text-decoration: none;
            color: white;
            display: block;
        }

        .sidenav2 {
            height: auto;
            padding-bottom: 20px;
            width: 0;
            position: fixed;
            z-index: 1000;
            top: 0;
            background-color: #111;
            overflow-x: hidden;
            transition: 0.5s;
            padding-top: 50px;
            text-align: right;
            direction: rtl;
            display: block;
        }

        #main2 {
            display: block
        }

        p {
            margin: 4px 10px 4px;
        }

        div.res_data_user {
            display: block;
        }

        hr {
            display: block;
        }

        /* ------------- MENU RESPONSIVE ---------------- */
        /* ------------- RESPONSIVE USER PANEL ---------------- */
        .margin {
            margin: 0 0 0 0 !important;
        }

        div.row {
            margin-left: 0;
            margin-right: 0;
        }

        .my-85 {
            margin-top: 70px;
        }

        div.col-md-4, div.col-md-8 {
            width: auto !important;
        }

        div.collapse {
            display: none;
        }

        /* ------------- RESPONSIVE USER PANEL ---------------- */
        /* ------------- RESPONSIVE DATA USER ---------------- */
        .table {
            display: none;
        }

        /* ------------- RESPONSIVE DATA USER ---------------- */

    }

    /* ------------------------ RESPONSIVE --------------------------- */


</style>

<body style="background:#e2e2e2!important;font-family: inherit;
    font-size: 17px;" id="body">
@include('frontend.partials.nav')
@include('frontend.partials.nav2')
<div id="margin" class="margin">
    <div class="row">
        <div class="col-xs-12 col-md-12 my-85">
            <div class="panel text-right" style="background: #fff;
    border: 1px #b9b9b9 solid;">
                <div class="panel-heading"
                     style="display: flex;font-size: x-large;">{{ isset($panel_title) ? $panel_title : ''  }}</div>
                <div class="panel-body" style="display: block;">
                    @yield('content')
                </div>
            </div>

        </div>
        {{--<div class="col-xs-12 col-md-3">
            <div class="panel text-right" style="background: #222;
border: 1px #333 solid;">
                <div class="panel-heading"
                     style="display: flex;font-size: x-large;">{{ isset($panel_title2) ? $panel_title2 : ''  }}</div>
                <div class="panel-body" style="display: block;">
                    @yield('content2')
                </div>
            </div>

        </div>--}}
    </div>
</div>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "200px";
        document.getElementById("main").style.marginLeft = "250px";
        document.getElementById("margin").style.marginRight = "220px";
        //document.getElementById("main").style.zIndex = "0";
    }

    function openNav2() {
        document.getElementById("mySidenav2").style.width = "100%";
        document.getElementById("main2").style.marginLeft = "250px";
        //document.getElementById("main").style.zIndex = "0";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft = "0";
        document.getElementById("margin").style.marginRight = "20px";
        //document.getElementById("main").style.zIndex = "2000";
    }

    function closeNav2() {
        document.getElementById("mySidenav2").style.width = "0";
        document.getElementById("main2").style.marginLeft = "0";
        //document.getElementById("main").style.zIndex = "2000";
    }

</script>
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.bundle.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/app.js"></script>
<script src="/js/frontend.js"></script>
</body>
</html>