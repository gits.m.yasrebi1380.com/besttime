<nav class="navbar navbar-expand-md nav-fix">
    <a href="#" class="logo">
        <img src="/images/logo.png">
    </a>
    <a href="#" class="logo2">
        <img src="/images/logo2.png">
    </a>
    <div class="collapse navbar-collapse mx-5" id="navbarColor01">
        @if(\Illuminate\Support\Facades\Auth::check())

            <div class="dropdown" style="margin:4px 0 4px 28px !important;">
                <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ \Illuminate\Support\Facades\Auth::user()->name . '   خوش آمدید  ' }}
                </button>
                <div class="dropdown-menu text-right" aria-labelledby="dropdownMenuButton" style="right: 74%">
                    <a class="dropdown-item" href="{{ route('logout') }}">
                        خروج
                    </a>
                </div>
            </div>
        @else

            {{--<form class="form-inline" style="direction: ltr !important;     margin: 5px 0 5px 28px !important;">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-dark my-2 my-sm-0" type="submit">Search</button>
            </form>--}}
        @endif

    </div>
</nav>
