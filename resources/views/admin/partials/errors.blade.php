@if($errors->any())
    <div class="alert alert-danger" style="font-size: x-large;text-align: right;">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif