@extends('layouts.admin')
@section('content')

    <a class="btn" href="{{ Route('admin.category.create') }}">
        <button class="btn btn-success">ایجاد دسته بندی جدید</button>
    </a>
    <hr>
    @include('admin.partials.notifications')
    @if($category && count($category) > 0)
        <table class="table table-bordered text-right">
            <thead class="btn-primary">
            <tr>
                <th class="text-right">شناسه</th>
                <th class="text-right">نام</th>
                <th class="text-right">عملیات</th>
            </tr>
            </thead>
            @foreach($category as $categor)
                @include('admin.category.item',$categor)
            @endforeach
        </table>


        @foreach($category as $categor)

            <div class="res_data_user">
                <button class="btn btn-primary my-2 w-75">شناسه : {{ $categor->id }}</button>
                <br>

                <button class="btn btn-primary my-2 w-75">نام : {{$categor->name}}</button>
                <br>
                <button class="btn btn-outline-dark my-2 w-75">عملیات
                    : @include('admin.category.operation',$categor)</button>
                <br>
            </div>
            <hr>
            {{--@include('frontend.time.item',$tim)--}}
        @endforeach

    @endif
@endsection

