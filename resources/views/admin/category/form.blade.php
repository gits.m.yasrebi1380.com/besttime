<div class="row" style="display: grid !important;">
    @include('admin.partials.errors')
    <div class="col-xs-10 col-md-4" style="width: 700px;text-align: right;font-size: large;">
        <form action="" method="post">
            {{ csrf_field()  }}
            <div class="form-group">
                <label for="full_name">نام :</label>
                <input type="text" class="form-control" name="full_name" id="full_name"
                       value="{{ old('full_name',isset($categoryItem) ? $categoryItem->categories: '')  }}">
            </div>
            <div class="form-group">
                <button class="btn btn-success" type="submit">ذخیره اطلاعات</button>
            </div>
        </form>
    </div>
</div>
