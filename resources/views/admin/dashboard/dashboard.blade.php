@extends('layouts.admin')
@section('content')

    @include('admin.partials.notifications')
    @if($user_item && count($user_item) > 0)
        <table class="table table-bordered text-right">
            <thead class="btn-primary">
            <tr>
                <th class="text-right">شناسه</th>
                <th class="text-right">نام</th>
                <th class="text-right">ایمیل</th>
                <th class="text-right">عملیات</th>
            </tr>
            </thead>
            @foreach($user_item as $user)
                @include('admin.user.item',$user)
            @endforeach
        </table>


        @foreach($user_item as $user)

            <div class="res_data_user">
                <button class="btn btn-primary my-2 w-75">شناسه : {{ $user->id }}</button>
                <br>

                <button class="btn btn-primary my-2 w-75">نام : {{$user->name}}</button>
                <br>

                <button class="btn btn-primary my-2 w-75">ایمیل : {{$user->email}}</button>
                <br>

                <button class="btn btn-outline-dark my-2 w-75">عملیات
                    : @include('admin.user.operation',$user)</button>
                <br>
            </div>
            <hr>
            {{--@include('frontend.time.item',$tim)--}}
        @endforeach

    @endif

@endsection