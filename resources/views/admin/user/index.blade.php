@extends('layouts.admin')
@section('content')

    <a class="btn" href="{{ Route('admin.user.create') }}">
        <button class="btn btn-success">ایجاد کاربر جدید</button>
    </a>
    <hr>
    @include('admin.partials.notifications')
    @if($users && count($users) > 0)
        <table class="table table-bordered text-right">
            <thead class="btn-primary">
            <tr>
                <th class="text-right">شناسه</th>
                <th class="text-right">نام</th>
                <th class="text-right">ایمیل</th>
                <th class="text-right">عملیات</th>
            </tr>
            </thead>
            @foreach($users as $user)
                @include('admin.user.item',$user)
            @endforeach
        </table>



        @foreach($users as $user)

            <div class="res_data_user">
                <button class="btn btn-primary my-2 w-75">شناسه : {{ $user->id }}</button>
                <br>

                <button class="btn btn-primary my-2 w-75">نام : {{$user->name}}</button>
                <br>

                <button class="btn btn-primary my-2 w-75">ایمیل : {{$user->email}}</button>
                <br>

                <button class="btn btn-outline-dark my-2 w-75">عملیات
                    : @include('admin.user.operation',$user)</button>
                <br>
            </div>
            <hr>
            {{--@include('frontend.time.item',$tim)--}}
        @endforeach




    @endif
@endsection

