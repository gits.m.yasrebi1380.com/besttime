@extends('layouts.admin')
@section('content')

    @include('admin.partials.notifications')
    @if($time_user && count($time_user) > 0)
        <table class="table table-bordered text-right">
            <thead class="btn-primary">
            <tr>
                <th class="text-right">زمان شروع</th>
                <th class="text-right">زمان پایان</th>
                <th class="text-right">زمان صرف شده</th>
                <th class="text-right">تاریخ</th>
                <th class="text-right">توضیحات مربوطه</th>
                <th class="text-right">عملیات</th>
            </tr>
            </thead>
            @foreach($time_user as $tim)
                @include('admin.time.item',$tim)
            @endforeach
        </table>



        @foreach($time_user as $tim)

            <div class="res_data_user">
                <button class="btn btn-primary my-2 w-75">زمان شروع : {{$tim->started_at}}</button>
                <br>

                <button class="btn btn-primary my-2 w-75">زمان پایان : {{$tim->finished_at}}</button>
                <br>

                <button class="btn btn-primary my-2 w-75">زمان صرف شده : {{$tim->time_used_at}}</button>
                <br>

                <button class="btn btn-primary my-2 w-75">تاریخ : {{$tim->date_day}}</button>
                <br>

                <p style="margin: 0.5rem 0 0 0 !important;">
                    <a class="btn btn-primary w-75 dropdown-toggle" data-toggle="collapse" href="#multiCollapseExample2"
                       role="button" aria-expanded="false" aria-controls="multiCollapseExample2">توضیحات مربوطه : </a>
                </p>
                <div class="row" style="width: 233px">
                    <div class="col" style="padding: 0!important;">
                        <div class="collapse multi-collapse" id="multiCollapseExample2">
                            <button class="btn btn-dark w-100 text-break">
                                <p class="dropdown-item text-white text-break">
                                    {{$tim->description}}
                                </p>
                            </button>
                        </div>
                    </div>
                </div>
                <br>

                <button class="btn btn-primary my-2 w-75">وضعیت : @if( $tim->status == \App\Models\Time::FINISHED )
                        <a class="btn btn-success">تمام شده</a>
                    @else
                        <a class="btn btn-danger" data-toggle="tooltip" data-placement="top"
                           title="برای تکمیل لطفا از قسمت عملیات اقدام کنید">
                            ناتمام
                        </a>
                    @endif</button>
                <br>

                <button class="btn btn-outline-dark my-2 w-75">عملیات
                    : @include('admin.time.operation',$tim)</button>
                <br>
            </div>
            <hr>
            {{--@include('frontend.time.item',$tim)--}}
        @endforeach





    @else
        <table class="table table-bordered text-right">
            <thead class="btn-primary">
            <tr>
                <th class="text-right">زمان شروع</th>
                <th class="text-right">زمان پایان</th>
                <th class="text-right">زمان صرف شده</th>
                <th class="text-right">تاریخ</th>
                <th class="text-right">توضیحات مربوطه</th>
                <th class="text-right">عملیات</th>
            </tr>
            </thead>
            <tr>
                <td> هیچ زمان بندی ای برای این کاربر موجود نیست</td>
            </tr>
        </table>
    @endif
@endsection

