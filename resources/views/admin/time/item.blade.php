<tr>
    <td>{{ $tim->started_at  }}</td>
    <td>{{ $tim->finished_at  }}</td>
    <td>{{ $tim->time_used_at  }}</td>
    <td>{{ $tim->date_day  }}</td>
    <td class="text-break">{{ $tim->description  }}</td>
    <td style="text-align: center;">
        @include('admin.time.operation',$tim)
    </td>
</tr>